package com.hsn.cdd.cache;

import com.hsn.cdd.cache.bean.User;
import com.hsn.cdd.cache.dao.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootCacheApplicationTests {
    @Autowired
    UserDao dao;
    @Test
    public void contextLoads() {
        User user = dao.getUser(1);
        System.out.println(user.toString());
    }

}
