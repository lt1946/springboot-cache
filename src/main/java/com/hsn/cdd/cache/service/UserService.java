package com.hsn.cdd.cache.service;

import com.hsn.cdd.cache.bean.User;
import com.hsn.cdd.cache.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用户service
 *
 * @author 陈冬冬
 * @create 2018-07-09 22:41
 **/
@Service
public class UserService {

    @Autowired
    private UserDao userDao;




    /**@Cacheable：将方法返回值进行缓存。参数
     *  @Cacheable(cacheNames="user",key=#id)
     *  cacheNames =user :为该缓存取名叫user,如果有角色缓存，那么也可以@Cacheable(cacheNames="role") 表示不同的缓存组件
     *  key=#id,使用了SpEl表达式，代表使用该方法的参数id作为key,返回结果即为查询的结果。map键值对的操作。骚的一批
     *  key有很多东西作为key，比如方法名，第几个参数为key等等，具体的自己百度去。
     *  @Cacheable(cacheNames="user",key=#id,cacheManage=RedisCache)
     *  cacheManage:指定缓存管理器，有RedisCache，EhCache等等
     * @Cacheble(condition="#id>0")
     * condition:指定条件下才进行缓存。即id大于0的时候才进行缓存
     * @Cacheble(unless="#result==null")
     * unless:否定缓存，满足什么条件就不缓存了。
     * #result等于查询出来的结果。上述语句的意思为如果查询结果为null,就不进行缓存了。简直牛的一批
     * @Cacheble(sync=true)
     * sync:异步缓存模式
     * @Cacheble(keyGenerator="myKeyGenerator")
     * keyGenerator="myKeyGenerator":指定自己的cache key生产策略
     * myKeyGenerator：自定义的key生成器,对应MyKeyGenerator类中的配置类
     * @Cacheble 运行过程：先去在缓存中找有没有，有就用缓存，没有就执行方法。跟@CachePut的流程不一样
     *
     * */
    @Cacheable(cacheNames = "user",key="#id")
    public User getUserInfo(Integer id){
        System.out.println("查询id为"+id+"的用户");
        return userDao.getUser(id);
    }
    /**
     * @CachePut :既调用方法，又更新缓存
     * 运行过程：先调用目标方法，然后缓存结果
     * */
    @CachePut(value = "user",key = "#result.pk_id")
    public User update(User user){
        System.out.println("调用了更新用户方法===========");
        userDao.updateUser(user);
        return user;
    }

    /**
     * @CacheEvict  清除缓存，多用于删除数据的时候，同时删除缓存
     *  可通过key指定清除的缓存。如果不指定，则默认使用参数作为key，也就是删除你传入的id，key=id那个cache
     * @CacheEvict(value="user" allEntries = true)
     *  allEntries = true:是否删除缓存名为user的所有数据。默认为false
     * @CacheEvict(value = "user",beforeInvocation = true)
     *  beforeInvocation = true : 是否在方法执行之前清空缓存。
     *  CacheEvict默认是在方法执行完之后才进行缓存的清空。如果在删除用户的过程中出现错误，那么缓存就不会被清空，
     *  如果是要在方法执行之前，不管方法执行成功与否，都要清空缓存，那么则可以使用该属性
     *
     * */
    @CacheEvict(value = "user",beforeInvocation = true)
    public String delUser(Integer id){
        int i = userDao.delUser(id);
        if(i==1){
            return "delete success!";
        }
        return "delete fail";
    }

}