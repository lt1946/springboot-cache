package com.hsn.cdd.cache.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Cache key生成器
 *
 * @author 陈冬冬
 * @create 2018-07-10 22:21
 **/
@Configuration
public class MyKeyGenerator{

    @Bean("myGeneratorKey")
    public KeyGenerator getKeyGenerator(){
        return new KeyGenerator() {
            @Override
            public Object generate(Object o, Method method, Object... objects) {
                return method.getName()+"ever"+ Arrays.asList(objects).toString();
            }
        };
    }

}