package com.hsn.cdd.cache.dao;

import com.hsn.cdd.cache.bean.User;
import org.apache.ibatis.annotations.*;

/**
 * 用户查询接口
 *
 * @author 陈冬冬
 * @create 2018-07-09 22:30
 **/
@Mapper
public interface UserDao {

    @Select("select * from c_user where pk_id=#{pk_id}")
     User getUser(Integer id);

    @Update("update c_user set password =#{password} where pk_id =#{pk_id}")
    void updateUser(User user);

    @Delete("delete from c_user where pk_id=#{pk_id}")
    int delUser(Integer pk_id);

    @Insert("insert into c_user(username,password,qq_number) values(#{username},#{password},#{qq_number})")
    void insertUser(User user);
}