package com.hsn.cdd.cache.bean;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.sql.Date;

/**
 * 用户
 *
 * @author 陈冬冬
 * @create 2018-07-09 22:23
 **/
@Data
public class User implements Serializable {
        private Integer pk_id;
        private String username;
        private String password;
        private String ip;
        private Date createTime;
        private String qqNumber;
        private Integer isStatus;
        private String salt;

}