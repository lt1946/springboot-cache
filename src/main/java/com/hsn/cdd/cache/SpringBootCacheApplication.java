package com.hsn.cdd.cache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
*  1.整合mybatis
 *  2.使用注解版的mybatis :用法：2.1使用MapperScan指定需要扫描的dao接口所在的包
 *  3.使用缓存：3.1开启基于注解缓存{@EnableCaching} 3.2标注缓存注解
 *  4. 缓存注解说明：（关于@Cacheable的参数都在service层有相关说明）
 *     4.1  @Cacheable:主要针对方法标注，对其方法的结果进行缓存,主要作用于查询
 *     4.2  @CacheEvict : 清空缓存，主要针对于删除
 *     4.3  @CachePut : 保证方法会执行并进行缓存。主要作用于更新
 *     4.4  @EnableCaching 开启基于注解的缓存
 * */
@MapperScan("com.hsn.cdd.cache.dao")
@SpringBootApplication
@EnableCaching
public class SpringBootCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCacheApplication.class, args);
    }
}
