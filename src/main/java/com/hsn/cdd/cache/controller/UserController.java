package com.hsn.cdd.cache.controller;
import com.hsn.cdd.cache.bean.User;
import com.hsn.cdd.cache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户控制器
 *
 * @author 陈冬冬
 * @create 2018-07-09 22:43
 **/
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/getUser/{id}")
    public User getUser(@PathVariable("id")Integer id){
        return userService.getUserInfo(id);
    }

    @RequestMapping("/updateUser")
    public User updateUser(User user){
        return userService.update(user);
    }

    @RequestMapping("/delUser/{pk_id}")
    public String delUser(@PathVariable("pk_id")Integer id){
        return userService.delUser(id);
    }
}